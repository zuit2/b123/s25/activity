/*
match: supplier: red farms
count

match: supplier: green farms
count

match: onSale
group: supplier, avg price

match: {}
group: supplier, max price

match: {}
group: supplier, min price


*/


db.fruits.aggregate([
	{$match: {supplier:"Red Farms Inc"}},
	{$count: "amountOfItemsByRedFarm"}
])

db.fruits.aggregate([
	{$match: {supplier:"Green Farming and Canning"}},
	{$count: "amountOfItemsByGreenFarm"}
])

db.fruits.aggregate([
	{$match: {onSale:true}},
	{$group: {_id:"$supplier", avgPrice: {$avg: "$price"}}}
])

db.fruits.aggregate([
	{$match: {onSale:true}},
	{$group: {_id:"$supplier", maxPrice: {$max: "$price"}}}
])

db.fruits.aggregate([
	{$match: {onSale:true}},
	{$group: {_id:"$supplier", minPrice: {$min: "$price"}}}
])